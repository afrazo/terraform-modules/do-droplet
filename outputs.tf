output "droplet_ip" {
  value = digitalocean_droplet.this.ipv4_address
}

output "droplet_urn" {
  value = digitalocean_droplet.this.urn
}

output "droplet_status" {
  value = digitalocean_droplet.this.status
}

output "droplet_volume_ids" {
  value = digitalocean_droplet.this.volume_ids
}

output "firewall_id" {
  value = digitalocean_firewall.this.id
}

output "firewall_status" {
  value = digitalocean_firewall.this.status
}

output "firewall_name" {
  value = digitalocean_firewall.this.name
}

output "firewall_droplet_ids" {
  value = digitalocean_firewall.this.droplet_ids
}
