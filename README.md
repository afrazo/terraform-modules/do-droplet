Create Droplets in Digital Ocean.

## Usage

```hcl
module "server1" {
  source = "git::ssh://git@gitlab.com/afrazo/terraform-modules/do-droplet?ref=0.0.1"

  ssh_keys              = "some_existing_key"
  tags                  = ["secret-webserver"]

  infra_repo = {
    username = "you"
    password = "password"
    repo     = "repo_with_ansible"
  }

  droplet_configuration = secret_webserver_configuration = {
                          "name"   = "secret-webserver"
                          "region" = "ams3"
                          "size"   = "s-1vcpu-1gb"
                          "image"  = "ubuntu-20-04-x64"
                        }

  firewall_rules        = [
    {
      protocol         = "tcp"
      port_range       = "22"
      source_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol         = "tcp"
      port_range       = "80"
      source_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol         = "tcp"
      port_range       = "443"
      source_addresses = ["0.0.0.0/0", "::/0"]
    }
  ]
}
```
