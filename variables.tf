variable "default_tag_id" {
  description = "ID of an existing tag you'd like to also use"
  default     = null
}

variable "tags" {
  description = "Auxiliary tags to attach to the resources"
  type        = list(string)
  default     = []
}

variable "infra_repo" {
  description = "Map with [repo] URL, and [username] and [password] for embedding into it as basic auth"
  type        = map(string)
}

variable "droplet_configuration" {
  description = "Map with [name], [image], [region], and [size]"
  type        = map(string)
}

variable "ssh_keys" {
  description = "Keys to add to the initial droplet user (which is currently deleted after provisioning through Ansible)"
  type        = list(string)
}

variable "firewall_rules" {
  description = "List of maps each with [port_range], [protocal], and [source_address]. Optionally: [source_droplet_ids], [source_load_balancer_uids], [source_tags]"
  type        = list(any)
}

locals {
  tags = var.default_tag_id == null ? [for this_id in digitalocean_tag.aux_tags : this_id.id] : concat([var.default_tag_id], [for this_id in digitalocean_tag.aux_tags : this_id.id])
}
