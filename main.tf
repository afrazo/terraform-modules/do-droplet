resource "digitalocean_tag" "aux_tags" {
  for_each = toset(var.tags)

  name = each.key
}

resource "digitalocean_droplet" "this" {
  name               = var.droplet_configuration["name"]
  image              = var.droplet_configuration["image"]
  region             = var.droplet_configuration["region"]
  size               = var.droplet_configuration["size"]
  monitoring         = true
  private_networking = true
  ssh_keys           = var.ssh_keys
  tags               = local.tags

  provisioner "remote-exec" {
    inline = [
      "until apt-get update; do echo 'Could not get lock on apt, sleeping and trying again'; sleep 5; done && apt-get upgrade -y && apt-get install -y python3-pip",
      "sudo -H pip3 install ansible && sudo -H ansible-pull --url='https://${var.infra_repo["username"]}:${var.infra_repo["password"]}@${var.infra_repo["repo"]}' ansible/playbooks/base_play.yml",
    ]
  }
}

resource "digitalocean_firewall" "this" {
  name        = "secret-webserver"
  droplet_ids = [digitalocean_droplet.this.id]

  dynamic "inbound_rule" {
    for_each = var.firewall_rules

    content {
      port_range                = inbound_rule.value.port_range
      protocol                  = inbound_rule.value.protocol
      source_addresses          = inbound_rule.value.source_addresses
      source_droplet_ids        = lookup(inbound_rule.value, "source_droplet_ids", null)
      source_load_balancer_uids = lookup(inbound_rule.value, "source_load_balancer_uids", null)
      source_tags               = lookup(inbound_rule.value, "source_tags", null)
    }
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  tags = local.tags
}
